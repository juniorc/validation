﻿
Imports System.Collections.Generic
Imports System.Windows.Forms

Public Class cValidation

    Public Forms As vForm
    Public User As vUser
    Public DataType As vData
    Public Comparer As vComparer
    Public Check As vCheck

    Sub New()
        Forms = New vForm
        User = New vUser
        DataType = New vData
        Comparer = New vComparer
        Check = New vCheck
    End Sub

    Function getMensagens() As List(Of String)
        Return mensagens
    End Function

    Public Overrides Function ToString() As String
        Dim str As String = ""
        For Each s As String In mensagens
            str &= s & vbCrLf
        Next
        Return str
    End Function


    Function validate() As Boolean
        mensagens.Clear()
        Return Forms.validate() And User.validate() And Check.validate()
    End Function

    Private Sub MudaCor(sender As Object, e As EventArgs)
        'If sender.backgroundcolor = Drawing.Color.Black Then
        '    sender.forecolor = Drawing.Color.White
        'ElseIf sender.backgroundcolor = Drawing.Color.White Then
        '    sender.forecolor = Drawing.Color.Black
        'End If

    End Sub




End Class



