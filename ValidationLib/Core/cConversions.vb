﻿Imports System.Windows.Forms

Friend Class cConversions


    Shared Function Textbox(ByVal obj As Object) As TextBox
        If TypeOf obj Is TextBox Then
            Return CType(obj, TextBox)
        Else
            Return New TextBox
        End If
    End Function

    Shared Function MaskedTextBox(ByVal obj As Object) As MaskedTextBox
        If TypeOf obj Is MaskedTextBox Then
            Return CType(obj, MaskedTextBox)
        Else
            Return New MaskedTextBox
        End If
    End Function

    Shared Function NumericUpDown(ByVal obj As Object) As NumericUpDown
        If TypeOf obj Is NumericUpDown Then
            Return CType(obj, NumericUpDown)
        Else
            Return New NumericUpDown
        End If
    End Function

    Shared Function Combobox(ByVal obj As Object) As ComboBox
        If TypeOf obj Is ComboBox Then
            Return CType(obj, ComboBox)
        Else
            Return New ComboBox
        End If
    End Function

    Shared Function CheckBox(ByVal obj As Object) As CheckBox
        If TypeOf obj Is CheckBox Then
            Return CType(obj, CheckBox)
        Else
            Return New CheckBox
        End If
    End Function

    Shared Function RadioButton(ByVal obj As Object) As RadioButton
        If TypeOf obj Is RadioButton Then
            Return CType(obj, RadioButton)
        Else
            Return New RadioButton
        End If
    End Function

    Shared Function DateTimePicker(ByVal obj As Object) As DateTimePicker
        If TypeOf obj Is DateTimePicker Then
            Return CType(obj, DateTimePicker)
        Else
            Return New DateTimePicker
        End If
    End Function

    Shared Function MonthCalendar(ByVal obj As Object) As MonthCalendar
        If TypeOf obj Is MonthCalendar Then
            Return CType(obj, MonthCalendar)
        Else
            Return New MonthCalendar
        End If
    End Function


    Shared Function rcField(ByVal obj As Object) As cField
        If TypeOf obj Is cField Then
            Return CType(obj, cField)
        Else
            Return New cField
        End If
    End Function

End Class
