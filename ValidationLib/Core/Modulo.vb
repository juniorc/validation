﻿Imports System.Windows.Forms

Friend Module Modulo

    Friend mensagens As New List(Of String)

    Function retString(obj As Object) As String

        If TypeOf obj Is TextBox Then
            Return cConversions.Textbox(obj).Text
        ElseIf TypeOf obj Is MaskedTextBox Then
            Return cConversions.MaskedTextBox(obj).Text
        ElseIf TypeOf obj Is ComboBox Then
            Return cConversions.Combobox(obj).SelectedText
        ElseIf TypeOf obj Is NumericUpDown Then
            Return cConversions.NumericUpDown(obj).Value.ToString
        Else
            Return ""
        End If

    End Function

    Function retNumber(obj As Object) As Decimal
        If TypeOf obj Is TextBox Then
            Dim str As String = cConversions.Textbox(obj).Text
            If IsNumeric(str) Then
                Return CInt(str)
            Else
                Return 0
            End If
        ElseIf TypeOf obj Is NumericUpDown Then
            Return cConversions.NumericUpDown(obj).Value
        ElseIf TypeOf obj Is ComboBox Then
            Return cConversions.Combobox(obj).SelectedValue
        Else
            Return 0
        End If
    End Function

    Function retBoolean(obj As Object) As Boolean
        If TypeOf obj Is CheckBox Then
            Return cConversions.CheckBox(obj).Checked
        ElseIf TypeOf obj Is RadioButton Then
            Return cConversions.RadioButton(obj).Checked
        Else
            Return False
        End If
    End Function

    Function retDate(obj As Object) As DateTime
        If TypeOf obj Is CheckBox Then
            Return cConversions.DateTimePicker(obj).Value
        ElseIf TypeOf obj Is RadioButton Then
            Return cConversions.MonthCalendar(obj).SelectionRange.Start
        Else
            Return Now
        End If
    End Function

End Module
