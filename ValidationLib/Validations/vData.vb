﻿Public Class vData

    Private datatype As Hashtable
    Private values As Hashtable
    Private mensagens As New List(Of String)

    Sub New()
        datatype = New Hashtable
        values = New Hashtable
    End Sub

    Public Enum DataTypeRules
        [date]
        [integer]
        [string]
        [boolean]
    End Enum

    Sub addDataTypeRule(ByVal c As cField, ByVal regra As DataTypeRules)
        datatype(c) = regra
    End Sub

End Class
