﻿Imports System.Windows.Forms

Public Class vUser

    Private user As Hashtable
    Private values As Hashtable

    Sub New()
        user = New Hashtable
        values = New Hashtable
    End Sub

    Public Enum UserRules
        cnpj
        cpf
        email
        url
    End Enum

    Sub addRule(ByVal c As cField, ByVal regra As UserRules)
        user(c) = regra
    End Sub

    Function validate() As Boolean

        Dim erros As Integer = 0

        For Each pair As DictionaryEntry In user

            'email
            If CType(pair.Value, UserRules).Equals(UserRules.email) AndAlso Not cFunctions.ValidaEmail(retString(cConversions.rcField(pair.Key).objeto)) Then

                Dim c As cField = cConversions.rcField(pair.Key)
                c.showMessage = True
                mensagens.Add(String.Format("O campo {0} não é um email válido!", c.getNome))
                erros += 1

            ElseIf CType(pair.Value, UserRules).Equals(UserRules.cpf) AndAlso Not cFunctions.ValidaCPF(retString(cConversions.rcField(pair.Key).objeto)) Then

                Dim c As cField = cConversions.rcField(pair.Key)
                c.showMessage = True
                mensagens.Add(String.Format("O campo {0} não é um CPF válido!", c.getNome))
                erros += 1

            ElseIf CType(pair.Value, UserRules).Equals(UserRules.cnpj) AndAlso Not cFunctions.ValidaCNPJ(retString(cConversions.rcField(pair.Key).objeto)) Then

                Dim c As cField = cConversions.rcField(pair.Key)
                c.showMessage = True
                mensagens.Add(String.Format("O campo {0} não é um CNPJ válido!", c.getNome))
                erros += 1

            ElseIf CType(pair.Value, UserRules).Equals(UserRules.url) AndAlso Not cFunctions.validaURL(retString(cConversions.rcField(pair.Key).objeto)) Then

                Dim c As cField = cConversions.rcField(pair.Key)
                c.showMessage = True
                mensagens.Add(String.Format("O campo {0} não é uma URL válida!", c.getNome))
                erros += 1

            End If

        Next

        Return Not erros > 0

    End Function

End Class
