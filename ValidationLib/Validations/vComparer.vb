﻿Public Class vComparer

    Private comparer As Hashtable
    Private values As Hashtable
    Private mensagens As New List(Of String)

    Sub New()
        comparer = New Hashtable
        values = New Hashtable
    End Sub

    Public Enum ComparerRules
        maiorQue
        menorQue
        igual
        diferente
        maiorIgual
        menorIgual
    End Enum

    Sub addRule(campo As Object, nomeCampo As String, ByVal regra As ComparerRules, valor As Integer)
        Dim c As New cField()
        comparer(c) = regra
        values(c) = valor
    End Sub

    Function validate() As Boolean

        Dim erros As Integer = 0

        For Each pair As DictionaryEntry In comparer

            'email
            If CType(pair.Value, ComparerRules).Equals(ComparerRules.diferente) AndAlso retString(cConversions.rcField(pair.Key).objeto).Trim <> values(pair.Key) Then

                Dim c As cField = cConversions.rcField(pair.Key)
                c.showMessage = True
                mensagens.Add(String.Format("O campo {0} precisa ser ter um valor diferente de {0}!", c.getNome))
                erros += 1

            ElseIf CType(pair.Value, ComparerRules).Equals(ComparerRules.maiorQue) AndAlso Not cFunctions.ValidaCPF(retString(cConversions.rcField(pair.Key).objeto)) Then

                Dim c As cField = cConversions.rcField(pair.Key)
                c.showMessage = True
                mensagens.Add(String.Format("O campo {0} não é um CPF válido!", c.getNome))
                erros += 1

            ElseIf CType(pair.Value, ComparerRules).Equals(ComparerRules.maiorIgual) AndAlso Not cFunctions.ValidaCNPJ(retString(cConversions.rcField(pair.Key).objeto)) Then

                Dim c As cField = cConversions.rcField(pair.Key)
                c.showMessage = True
                mensagens.Add(String.Format("O campo {0} não é um CNPJ válido!", c.getNome))
                erros += 1

            ElseIf CType(pair.Value, ComparerRules).Equals(ComparerRules.menorQue) AndAlso Not cFunctions.validaURL(retString(cConversions.rcField(pair.Key).objeto)) Then

                Dim c As cField = cConversions.rcField(pair.Key)
                c.showMessage = True
                mensagens.Add(String.Format("O campo {0} não é uma URL válida!", c.getNome))
                erros += 1

            End If

        Next

        Return Not erros > 0

    End Function


End Class
