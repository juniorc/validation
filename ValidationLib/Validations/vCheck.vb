﻿Imports System.Windows.Forms

Public Class vCheck

    Private Shared mensagens As New List(Of String)

    Private check As Hashtable
    Private values As Hashtable

    Sub New()
        check = New Hashtable
        values = New Hashtable
    End Sub

    Public Enum CheckRules
        peloMenosUm
        noMaximo
        noMinimo
    End Enum

    Sub addRule(campos As CheckBox(), nomeGrupo As String, regra As CheckRules)
        Dim c As New cField(campos, nomeGrupo)
        check(c) = regra
    End Sub

    Sub addRule(campos As RadioButton(), nomeGrupo As String, regra As CheckRules)
        Dim c As New cField(campos, nomeGrupo)
        check(c) = regra
    End Sub

    Sub addRule(campos As CheckBox(), nomeGrupo As String, regra As CheckRules, minMax As Integer)
        Dim c As New cField(campos, nomeGrupo)
        check(c) = regra
        values(c) = minMax
    End Sub

    Sub addRule(campos As RadioButton(), nomeGrupo As String, regra As CheckRules, minMax As Integer)
        Dim c As New cField(campos, nomeGrupo)
        check(c) = regra
        values(c) = minMax
    End Sub

    Function validate() As Boolean

        Dim erros As Integer = 0

        For Each pair As DictionaryEntry In check

            Dim result As Boolean = False

            'atleastone
            If CType(pair.Value, CheckRules).Equals(CheckRules.peloMenosUm) Then

                result = False
                For Each c As Object In CType(pair.Key, cField).objeto
                    result = result Or retBoolean(c)
                Next

                If Not result Then
                    mensagens.Add(String.Format("O grupo {0} precisa de pelo menos uma opção selecionada!", CType(pair.Key, cField).getNome))
                    erros += 1
                End If

            ElseIf CType(pair.Value, CheckRules).Equals(CheckRules.noMaximo) Then

                Dim selecionados As Integer = 0

                For Each c As Object In CType(pair.Key, cField).objeto
                    If retBoolean(c) Then selecionados += 1
                Next

                If selecionados > values(pair.Key) Then
                    mensagens.Add(String.Format("O grupo {0} precisa de no máximo {1} opções selecionadas!", CType(pair.Key, cField).getNome, values(pair.Key)))
                    erros += 1
                End If

            ElseIf CType(pair.Value, CheckRules).Equals(CheckRules.noMinimo) Then

                Dim selecionados As Integer = 0

                For Each c As Object In CType(pair.Key, cField).objeto
                    If retBoolean(c) Then selecionados += 1
                Next

                If selecionados < values(pair.Key) Then
                    mensagens.Add(String.Format("O grupo {0} precisa de no mínimo {1} opções selecionadas!", CType(pair.Key, cField).getNome, values(pair.Key)))
                    erros += 1
                End If

            End If


        Next

        Return Not erros > 0

    End Function

End Class
