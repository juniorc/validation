﻿Imports System.Windows.Forms

Public Class vForm

    Private form As Hashtable
    Private values As Hashtable

    Sub New()
        form = New Hashtable
        values = New Hashtable
    End Sub

    Public Enum FormRules
        required
        requiredvalue
        maxlength
        rangelenght
    End Enum

    Function validate() As Boolean

        Dim erros As Integer = 0

        For Each pair As DictionaryEntry In form

            'required
            If CType(pair.Value, FormRules).Equals(FormRules.required) Then

                'textbox
                If retString(cConversions.rcField(pair.Key).objeto) = "" Then
                    Dim c As cField = cConversions.rcField(pair.Key)
                    Dim t As TextBox = cConversions.Textbox(c.objeto)
                    t.ForeColor = System.Drawing.Color.Red
                    'AddHandler t.LostFocus, AddressOf MudaCor

                    c.showMessage = True
                    mensagens.Add(String.Format("O campo {0} é obrigatório!", c.getNome))
                    erros += 1
                End If


                'required value
            ElseIf CType(pair.Value, FormRules).Equals(FormRules.requiredvalue) Then

                'textbox
                If retNumber(cConversions.rcField(pair.Key).objeto) <> values(pair.Key) Then

                    Dim c As cField = cConversions.rcField(pair.Key)
                    Dim t As TextBox = cConversions.Textbox(c.objeto)
                    t.ForeColor = Drawing.Color.Red
                    'AddHandler t.LostFocus, AddressOf MudaCor

                    c.showMessage = True
                    mensagens.Add(String.Format("O campo {0} deve ser igual a {1}!", c.getNome, values(pair.Key)))
                    erros += 1
                End If

            ElseIf CType(pair.Value, FormRules).Equals(FormRules.maxlength) Then

                'textbox
                If retString(cConversions.rcField(pair.Key).objeto).Length <= values(pair.Key) Then

                    Dim c As cField = cConversions.rcField(pair.Key)
                    Dim t As TextBox = cConversions.Textbox(c.objeto)
                    t.ForeColor = Drawing.Color.Red
                    'AddHandler t.LostFocus, AddressOf MudaCor

                    c.showMessage = True
                    mensagens.Add(String.Format("O campo {0} é deve ter até {1} caracteres!", c.getNome, values(pair.Key)))
                    erros += 1
                End If


            ElseIf CType(pair.Value, FormRules).Equals(FormRules.rangelenght) Then

                'textbox
                If retString(cConversions.rcField(pair.Key).objeto).Length >= values(pair.Key)(0) And retString(cConversions.rcField(pair.Key).objeto).Length <= values(pair.Key)(1) Then

                    Dim c As cField = cConversions.rcField(pair.Key)
                    Dim t As TextBox = cConversions.Textbox(c.objeto)
                    t.ForeColor = Drawing.Color.Red
                    'AddHandler t.LostFocus, AddressOf MudaCor

                    c.showMessage = True
                    mensagens.Add(String.Format("O campo {0} deve estar entre {1} e {2} caracteres!", c.getNome, values(pair.Key)))
                    erros += 1
                End If

            End If

        Next

        Return Not erros > 0

    End Function

    Sub addRule(ByVal c As cField, ByVal regra As vForm.FormRules, ByVal stringValue As String)
        form(c) = regra
        values(c) = stringValue
    End Sub

    Sub addRule(ByVal c As cField, ByVal regra As vForm.FormRules, ByVal intValue As Integer)
        form(c) = regra
        values(c) = intValue
    End Sub

    Sub addRule(ByVal c As cField, ByVal regra As vForm.FormRules, ByVal intValue() As Integer)
        form(c) = regra
        values(c) = intValue
    End Sub

    Sub addRule(ByVal c As cField, ByVal regra As vForm.FormRules)
        form(c) = regra
    End Sub

End Class
