﻿Imports ValidationLib

Public Class FrmTeste

    Dim v As ValidationLib.cValidation

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Configuração Validation
        v = New ValidationLib.cValidation
        v.Forms.addRule(New cField(txtNome, "Nome"), vForm.FormRules.required)
        v.User.addRule(New cField(txtEmail, "Email"), vUser.UserRules.email)
        v.User.addRule(New cField(txtCPF, "CPF"), vUser.UserRules.cpf)
        v.Forms.addRule(New cField(txtIdade, "Idade"), vForm.FormRules.requiredvalue, 10)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        If v.validate Then
            MsgBox("Sucesso!")
        Else
            MsgBox(v.ToString)
        End If

    End Sub
End Class
